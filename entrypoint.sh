#!/bin/bash
set -e

# vaciamos directorio /var/www/html


if [ -f /var/www/html/index.html ]; then
    rm -r /var/www/html/*
fi

# Fijarse si el directorio '/etc/apache2' esta vacio

if [ -z "$(ls -A /etc/apache2)" ]; then

    #echo "El directorio esta vacio, se copian ficheros desde '/app/apache2'"
    cp -r /app/apache2/* /etc/apache2
fi


# Modificar los valores del fichero default.conf 'ServerAlias' y 'ServerName'

sed -i 's/APPSERVERNAME/'"$APPSERVERNAME"'/' /app/apache2/sites-available/default.conf
sed -i 's/APPALIAS/'"$APPALIAS"'/' /app/apache2/sites-available/default.conf
cp /app/apache2/sites-available/default.conf /etc/apache2/sites-available/default.conf

# Activar sitio

a2ensite default.conf

# Iniciar mysql server para poder ejecutar los comandos posteriores
/etc/init.d/mysql start


#Esperamos 5 segundos para que cargue mysql
sleep 5

#Contraseña de root
#en bash ls comparaciones se hacen de dos formas
#para enteros '-eq'
#para cadena de caracteres '=='
if [ ! -f /app/mysql.configured ]; then
        if  [ $MYSQL_USER_PASSWORD == "1234" ]; then
                RPASS=$((10000 + $RANDOM %30000))

                mysql -u root -e "CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$RPASS';"
                touch /app/mysql.configured
        else
                mysql -u root -e "CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$MYSQL_USER_PASSWORD';"
                touch /app/mysql.configured
        fi
fi

if [ ! -f /app/inventario.configured ]; then
        cd /tmp/
        mv /tmp/sistema-web/* /var/www/html/
        sed -i 's/database_name_here/'"$MYSQL_DB_NAME"'/' /var/www/html/modelo/modelo_conexion.php
        sed -i 's/username_here/'"$MYSQL_USER"'/' /var/www/html/modelo/modelo_conexion.php
        sed -i 's/password_here/'"$MYSQL_USER_PASSWORD"'/' /var/www/html/modelo/modelo_conexion.php
        touch /app/inventario.configured
fi


#Crear base de datos

if [ ! -f /app/bd.configured ]; then
        if [ $MYSQL_DB_NAME == "inventario" ]; then
                mysql -u root -e "CREATE DATABASE $MYSQL_DB_NAME;"
                mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'localhost';"
                mysql -u $MYSQL_USER -p$MYSQL_USER_PASSWORD $MYSQL_DB_NAME < /var/www/html/inventario.sql
                touch /app/bd.configured
        else
                wget https://gitlab.com/jdsantucho/php/-/raw/main/inventario.sql
                mysql -u root -e "CREATE DATABASE $MYSQL_DB_NAME;"
                mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'localhost';"
                mysql -u $MYSQL_USER -p$MYSQL_USER_PASSWORD $MYSQL_DB_NAME < /var/www/html/inventario.sql
                touch /app/bd.configured
        fi
fi


#Inicio de servicios

apachectl -D FOREGROUND

exec "$@"
