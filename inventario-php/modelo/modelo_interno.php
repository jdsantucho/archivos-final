<?php
    class Modelo_Interno{
        private $conexion;
        function __construct(){
            require_once 'modelo_conexion.php';
            $this->conexion = new conexion();
            $this->conexion->conectar();
        }

        
        function listar_interno(){
            $sql = "call SP_LISTAR_INTERNO()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_assoc($consulta)) {
                    $arreglo["data"][]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

        function Registrar_Interno($interno,$telefono){
            $sql = "call SP_REGISTRAR_INTERNO('$interno','$telefono')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }

        function Modificar_Interno($id,$internoactual,$internonueva,$telefono){
            $sql = "call SP_MODIFICAR_INTERNO('$id','$internoactual','$internonueva','$telefono')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }
        
    }
?>