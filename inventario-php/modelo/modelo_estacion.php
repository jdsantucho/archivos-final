<?php
    class Modelo_Estacion{
        private $conexion;
        function __construct(){
            require_once 'modelo_conexion.php';
            $this->conexion = new conexion();
            $this->conexion->conectar();
        }

        
        function listar_estacion(){
            $sql = "call SP_LISTAR_ESTACION()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_assoc($consulta)) {
                    $arreglo["data"][]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

        function Registrar_Estacion($estacion){
            $sql = "call SP_REGISTRAR_ESTACION('$estacion')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }

        function Modificar_Estacion($id,$estacionactual,$estacionnueva){
            $sql = "call SP_MODIFICAR_ESTACION('$id','$estacionactual','$estacionnueva')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }
        
    }
?>