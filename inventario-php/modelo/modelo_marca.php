<?php
    class Modelo_Marca{
        private $conexion;
        function __construct(){
            require_once 'modelo_conexion.php';
            $this->conexion = new conexion();
            $this->conexion->conectar();
        }

        
        function listar_marca(){
            $sql = "call SP_LISTAR_MARCA()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_assoc($consulta)) {
                    $arreglo["data"][]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

        function Registrar_Marca($marca,$estatus){
            $sql = "call SP_REGISTRAR_MARCA('$marca','$estatus')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }

        function Modificar_Marca($id,$marcaactual,$marcanueva,$estatus){
            $sql = "call SP_MODIFICAR_MARCA('$id','$marcaactual','$marcanueva','$estatus')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }
        
    }
?>