<?php

	require_once '../../modelo/modelo_lspersonal.php';

	$MC = new Modelo_Lspersonal();
	$idpersonal = htmlspecialchars($_POST['idpersonal'], ENT_QUOTES, 'UTF-8');
	$idestacion = htmlspecialchars($_POST['idestacion'], ENT_QUOTES, 'UTF-8');
	$idgerencia = htmlspecialchars($_POST['idgerencia'], ENT_QUOTES, 'UTF-8');
	$iddepartamento = htmlspecialchars($_POST['iddepartamento'], ENT_QUOTES, 'UTF-8');
	$estatus = htmlspecialchars($_POST['estatus'], ENT_QUOTES, 'UTF-8');
	$nombre = htmlspecialchars($_POST['nombre'], ENT_QUOTES, 'UTF-8');
	$interno = htmlspecialchars($_POST['interno'], ENT_QUOTES, 'UTF-8');
	$consulta = $MC->Editar_Lspersonal($idpersonal,$idestacion,$idgerencia,$iddepartamento,$estatus,$nombre,$interno);
	echo $consulta;

?>