<?php

	require_once '../../modelo/modelo_lspersonal.php';

	$MC = new Modelo_Lspersonal();
	$idestacion = htmlspecialchars($_POST['idestacion'], ENT_QUOTES, 'UTF-8');
	$idgerencia = htmlspecialchars($_POST['idgerencia'], ENT_QUOTES, 'UTF-8');
	$iddepartamento = htmlspecialchars($_POST['iddepartamento'], ENT_QUOTES, 'UTF-8');
	$nombrepersonal = htmlspecialchars($_POST['nombrepersonal'], ENT_QUOTES, 'UTF-8');
	$interno = htmlspecialchars($_POST['interno'], ENT_QUOTES, 'UTF-8');
	$consulta = $MC->Registrar_Lspersonal($idestacion,$idgerencia,$iddepartamento,$nombrepersonal,$interno);
	echo $consulta;

?>