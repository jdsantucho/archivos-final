<?php
    require '../../modelo/modelo_departamento.php';
    $MM = new Modelo_Departamento();
    $consulta = $MM->listar_departamento();
    if($consulta){
        echo json_encode($consulta);
    }else{
        echo '{
            "sEcho": 1,
            "iTotalRecords": "0",
            "iTotalDisplayRecords": "0",
            "aaData": []
        }';
    }

?>