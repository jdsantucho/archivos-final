<?php
    require '../../modelo/modelo_modelo.php';

    $ME = new Modelo_Modelo();
    $idmodelo = htmlspecialchars($_POST['idmodelo'],ENT_QUOTES,'UTF-8');
    $modeloactual = htmlspecialchars($_POST['moac'],ENT_QUOTES,'UTF-8');
    $modelonuevo = htmlspecialchars($_POST['monu'],ENT_QUOTES,'UTF-8');
    $marca = htmlspecialchars($_POST['marca'],ENT_QUOTES,'UTF-8');
    $consulta = $ME->Modificar_Modelo($idmodelo,$modeloactual,$modelonuevo,$marca);
    echo $consulta;

?>