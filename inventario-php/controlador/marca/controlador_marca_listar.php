<?php
    require '../../modelo/modelo_marca.php';
    $MM = new Modelo_Marca();
    $consulta = $MM->listar_marca();
    if($consulta){
        echo json_encode($consulta);
    }else{
        echo '{
            "sEcho": 1,
            "iTotalRecords": "0",
            "iTotalDisplayRecords": "0",
            "aaData": []
        }';
    }

?>