<?php
    require '../../modelo/modelo_estacion.php';
    $ME = new Modelo_Estacion();
    $consulta = $ME->listar_estacion();
    if($consulta){
        echo json_encode($consulta);
    }else{
        echo '{
            "sEcho": 1,
            "iTotalRecords": "0",
            "iTotalDisplayRecords": "0",
            "aaData": []
        }';
    }

?>