<script type="text/javascript" src="../js/departamento.js?rev=<?php echo time();?>"></script>
<form autocomplete="false" onsubmit="return false">
<div class="col-md-12">
    <div class="box box-success box-solid">
        <div class="box-header with-border">
              <h3 class="box-title">DEPARTAMENTOS</h3>

              <!-- /.box-tools -->
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                <div class="col-lg-10">
                  <div class="input-group">
                   <input type="text" class="global_filter form-control" id="global_filter" placeholder="Ingresar dato a buscar">
                   <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  </div>
              </div>
                <div class="col-lg-2">
                  <button class="btn btn-danger" style="width:100%" onclick="AbrirModalRegistro()"><i class="glyphicon glyphicon-plus"></i>Nuevo registro</button>
                  </div><br><br>
              </div>
              <table id="tabla_departamento" class="display responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Departamento</th>
                    <th>Gerencia</th>
                    <th>Acci&oacute;n</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>Departamento</th>
                    <th>Gerencia</th>
                    <th>Acci&oacute;n</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
    </div>
          <!-- /.box -->
</div>
</form>
<div class="modal fade" id="modal_registro" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Registro de departamento</b></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            
          <div class="col-lg-12">
          <label for="">Nombre departamento:</label>
          <input type="text" class="form-control" id="txt_direccion" placeholder="Ingrese direccion">
        </div><br><br><br>
        
        <div class="col-lg-12">
            <label for="">Gerencia:</label>
            <select class="js-states form-control" name="state" id="cbm_gerencia" style="width:100%;">
            </select><br><br>
          </div>
          
        </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Registrar_Departamento()"><i class="fa fa-check"> <b>Registrar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_editar" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Editar departamento</b></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            
          <div class="col-lg-12">
            <input type="text" id="id_departamento" hidden>
          </div>
          
          <div class="col-lg-12">

            <label for="">Nombre:</label>
            <input type="text" id="txt_departamento_actual_editar" maxlength="50" onkeypress="return soloLetras(event)" hidden>
            <input type="text" class="form-control" id="txt_departamento_nuevo_editar" placeholder="Ingrese modelo" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>

        <div class="col-lg-12">
            <label for="">Gerencia:</label>
            <select class="js-states form-control" name="state" id="cbm_gerencia_editar" style="width:100%;">
            </select><br><br>
          </div>
        </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Editar_Departamento()"><i class="fa fa-check"> <b>Modificar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>



<script>
$(document).ready(function(){
  listar_departamento();
  listar_combo_rol();
  listar_gerencia_combo();
    $('.js-states form-control').select2();
    $("#modal_registro").on('shown.bs.modal',function(){
      $("#txt_nombre").focus();
      
    })
});

$('.box').boxWidget({
  animationSpeed : 500,
  collapseTrigger : '[data-widget="collapse"]',
  removeTrigger : '[data-widget="remove"]',
  collapseIcon : 'fa-minus',
  expandIcon : 'fa-plus',
  removeIcon : 'fa-times'
})

</script>