<script type="text/javascript" src="../js/propietario.js?rev=<?php echo time();?>"></script>
<div class="col-md-12">
    <div class="box box-success box-solid">
        <div class="box-header with-border">
              <h3 class="box-title">PROPIETARIOS</h3>

            
              <!-- /.box-tools -->
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                <div class="col-lg-10">
                  <div class="input-group">
                   <input type="text" class="global_filter form-control" id="global_filter" placeholder="Ingresar dato a buscar">
                   <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  </div>
              </div>
                <div class="col-lg-2">
                  <button class="btn btn-danger" style="width:100%" onclick="AbrirModalRegistro()"><i class="glyphicon glyphicon-plus"></i>Nuevo registro</button><br><br>
                  </div>
              </div>
              <table id="tabla_propietario" class="display responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Fecha registro</th>
                    <th>Estatus</th>
                    <th>Acci&oacute;n</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Fecha registro</th>
                    <th>Estatus</th>
                    <th>Acci&oacute;n</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
    </div>
          <!-- /.box -->
</div>

<div class="modal fade" id="modal_registro" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header  bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Registro de propietario</b></h4>
        </div>

      <div class="row">
        <div class="modal-body">
          <div class="col-lg-12">
            <label for="">Nombre:</label>
            <input type="text" class="form-control" id="txt_propietario" placeholder="Ingrese propietario" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>
       
          <div class="col-lg-12">
            <label for="">Estatus:</label>
            <select class="js-states form-control" name="state" id="cbm_estatus" style="width:100%;">
              <option value="ACTIVO">ACTIVO</option>
              <option value="INACTIVO">INACTIVO</option>
            </select><br><br>
          </div>
          
        </div>

      </div>  
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Registrar_Propietario()"><i class="fa fa-check"> <b>Registrar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="modal_editar" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Editar de propietario</b></h4>
        </div>
        <div class="modal-body">
          <div class="row">
          <div class="col-lg-12">
            <input type="text" id="id_propietario" hidden>
            <label for="">Nombre:</label>
            <input type="text" id="txt_propietario_actual_editar" placeholder="Ingrese propietario" maxlength="50" onkeypress="return soloLetras(event)" hidden>
            <input type="text" class="form-control" id="txt_propietario_nueva_editar" placeholder="Ingrese propietario" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>
       
          <div class="col-lg-12">
            <label for="">Estatus:</label>
            <select class="js-states form-control" name="state" id="cbm_estatus_editar" style="width:100%;">
              <option value="ACTIVO">ACTIVO</option>
              <option value="INACTIVO">INACTIVO</option>
            </select><br><br>
          </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Editar_Propietario()"><i class="fa fa-check"> <b>Editar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>

<script>
$(document).ready(function(){
  listar_propietario();
    $('.js-states form-control').select2();
    $("#modal_registro").on('shown.bs.modal',function(){
      $("#txt_propietario").focus();
    })
});

$('.box').boxWidget({
  animationSpeed : 500,
  collapseTrigger : '[data-widget="collapse"]',
  removeTrigger : '[data-widget="remove"]',
  collapseIcon : 'fa-minus',
  expandIcon : 'fa-plus',
  removeIcon : 'fa-times'
})


</script>