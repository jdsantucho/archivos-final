<script type="text/javascript" src="../js/modelo.js?rev=<?php echo time();?>"></script>
<form autocomplete="false" onsubmit="return false">
<div class="col-md-12">
    <div class="box box-success box-solid">
        <div class="box-header with-border">
              <h3 class="box-title">MODELOS</h3>

            <!-- /.box-tools -->
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                <div class="col-lg-10">
                  <div class="input-group">
                   <input type="text" class="global_filter form-control" id="global_filter" placeholder="Ingresar dato a buscar">
                   <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  </div>
              </div>
                <div class="col-lg-2">
                  <button class="btn btn-danger" style="width:100%" onclick="AbrirModalRegistro()"><i class="glyphicon glyphicon-plus"></i>Nuevo registro</button><br><br>
                  </div>
              </div>
              <table id="tabla_modelo" class="display responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Acci&oacute;n</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Acci&oacute;n</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
    </div>
          <!-- /.box -->
</div>
</form>
<div class="modal fade" id="modal_registro" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Registro de modelo</b></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            
        <div class="col-lg-12">
          <label for="">Modelo:</label>
          <input type="text" class="form-control" id="txt_modelo" placeholder="Ingrese nombre modelo">
        </div><br><br><br>

        <div class="col-lg-12">
            <label for="">Marca:</label>
            <select class="js-states form-control" name="state" id="cbm_marca" style="width:100%;">
            </select><br><br>
          </div>

        </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Registrar_Modelo()"><i class="fa fa-check"> <b>Registrar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_editar" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Editar modelo</b></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            
          <div class="col-lg-12">
            <input type="text" id="id_modelo" hidden>
          </div>
          
          <div class="col-lg-12">

            <label for="">Nombre:</label>
            <input type="text" id="txt_modelo_actual_editar" maxlength="50" onkeypress="return soloLetras(event)" hidden>
            <input type="text" class="form-control" id="txt_modelo_nuevo_editar" placeholder="Ingrese modelo" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>

        <div class="col-lg-12">
            <label for="">Marca:</label>
            <select class="js-states form-control" name="state" id="cbm_marca_editar" style="width:100%;">
            </select><br><br>
          </div>
        </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Editar_Modelo()"><i class="fa fa-check"> <b>Modificar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>



<script>
$(document).ready(function(){
  listar_modelo();
  listar_marca_combo();
    $('.js-states form-control').select2();
    $("#modal_registro").on('shown.bs.modal',function(){
      $("#txt_modelo").focus();
      
    })
});

$('.box').boxWidget({
  animationSpeed : 500,
  collapseTrigger : '[data-widget="collapse"]',
  removeTrigger : '[data-widget="remove"]',
  collapseIcon : 'fa-minus',
  expandIcon : 'fa-plus',
  removeIcon : 'fa-times'
})

/*document.getElementById('txt_email').addEventListener('input',function(){
  campo=event.target;
  emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  if(emailRegex.test(campo.value)){
    $(this).css("border","1px solid green");
    $("#emailOK").html("");
    $("#validar_email").val("correcto");
  }else{
    $(this).css("border","1px solid red");
    $("#emailOK").html("Email incorrecto");
    $("#validar_email").val("incorrecto");
  }
});

document.getElementById('txt_email_editar').addEventListener('input',function(){
  campo=event.target;
  emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  if(emailRegex.test(campo.value)){
    $(this).css("border","1px solid green");
    $("#emailOK_editar").html("");
    $("#validar_email_editar").val("correcto");
  }else{
    $(this).css("border","1px solid red");
    $("#emailOK_editar").html("Email incorrecto");
    $("#validar_email_editar").val("incorrecto");
  }
});*/

</script>