DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_INTENTO_USUARIO`(IN `USUARIO` VARCHAR(50))
BEGIN
DECLARE INTENTO INT;
SET @INTENTO :=(SELECT usu_intento from usuario where usu_nombre=USUARIO);
if @INTENTO  = 3 THEN
	SELECT @INTENTO;
ELSE
	UPDATE usuario set 
	usu_intento=@INTENTO+1
	WHERE usu_nombre=USUARIO;
	SELECT @INTENTO;

END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_COMBO_GERENCIA`()
SELECT * FROM gerencia WHERE gerencia_estatus='ACTIVO'$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_COMBO_MARCA`()
SELECT * FROM marca WHERE marca_estatus='ACTIVO'$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_COMBO_ROL`()
SELECT
rol.rol_id,
rol.rol_nombre
FROM
rol$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_DEPARTAMENTO`()
SELECT
departamento.departamento_id,
departamento.departamento_direccion,
departamento.gerencia_id,
gerencia.gerencia_nombre
FROM
departamento
INNER JOIN gerencia ON departamento.gerencia_id = gerencia.gerencia_id$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_DEPARTAMENTO_COMBO`(IN `ID` INT)
SELECT departamento_id, departamento_direccion FROM departamento where `gerencia_id`=ID$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_ESTACION`()
SELECT * FROM estacion$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_ESTACION_COMBO`()
SELECT estacion_id,estacion_nombre FROM estacion$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_GERENCIA`()
SELECT * FROM gerencia$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_GERENCIA_COMBO`()
SELECT gerencia_id,gerencia_nombre FROM gerencia$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_IMPRESORA`()
SELECT c.impresora_id, c.impresora_feregistro, c.impresora_estatus, c.impresora_serie, c.impresora_mac, c.impresora_ip,impresora_contacto,impresora_info,
u.estacion_id, CONCAT_WS(' ',u.estacion_nombre) as estacion, 
gd.departamento_id, CONCAT_WS(' ',gd.departamento_direccion) as departamento, 
gg.gerencia_id, CONCAT_WS(' ',gg.gerencia_nombre) as gerencia,
p.propietario_id, CONCAT_WS(' ',p.propietario_nombre) as propietario, 
c.modelo_id, CONCAT_WS(' ',m.modelo_nombre) as modelo, 
e.marca_id, CONCAT_WS(' ',e.marca_nombre) as marca
FROM impresora as c
INNER JOIN propietario as p ON c.propietario_id=p.propietario_id
INNER JOIN modelo as m ON c.modelo_id=m.modelo_id
INNER JOIN marca as e on e.marca_id=m.marca_id

INNER JOIN estacion as u ON c.estacion_id=u.estacion_id
INNER JOIN departamento as gd ON c.departamento_id=gd.departamento_id
INNER JOIN gerencia as gg on gg.gerencia_id=gd.gerencia_id

ORDER BY impresora_id DESC$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_LSPERSONAL`()
SELECT c.lspersonal_id, c.lspersonal_feregistro, c.lspersonal_estatus, c.lspersonal_interno, c.lspersonal_nombre,
p.estacion_id, CONCAT_WS(' ',p.estacion_nombre) as estacion, 
c.departamento_id, CONCAT_WS(' ',m.departamento_direccion) as departamento, 
e.gerencia_id, CONCAT_WS(' ',e.gerencia_nombre) as gerencia
FROM lspersonal as c
INNER JOIN estacion as p ON c.estacion_id=p.estacion_id
INNER JOIN departamento as m ON c.departamento_id=m.departamento_id
INNER JOIN gerencia as e on e.gerencia_id=m.gerencia_id
ORDER BY lspersonal_id DESC$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_MARCA`()
SELECT * FROM marca$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_MARCA_COMBO`()
SELECT marca_id,marca_nombre FROM marca$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_MODELO`()
SELECT
modelo.modelo_id,
modelo.modelo_nombre,
modelo.marca_id,
marca.marca_nombre

FROM
modelo
INNER JOIN marca ON modelo.marca_id = marca.marca_id$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_MODELO_COMBO`(IN `ID` INT)
SELECT modelo_id, modelo_nombre FROM modelo where `marca_id`=ID$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_PROPIETARIO`()
SELECT * FROM propietario$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_PROPIETARIO_COMBO`()
SELECT propietario_id,propietario_nombre FROM propietario$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_LISTAR_USUARIO`()
BEGIN
DECLARE CANTIDAD int;
SET @CANTIDAD:=0;
SELECT
@CANTIDAD:=@CANTIDAD+1 AS posicion,
usuario.usu_id,
usuario.usu_nombre,
usuario.usu_sexo,
usuario.rol_id,
usuario.usu_estatus,
rol.rol_nombre,
usuario.usu_email
FROM
usuario
INNER JOIN rol ON usuario.rol_id = rol.rol_id;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_CONTRA_USUARIO`(IN `IDUSUARIO` INT, IN `CONTRA` VARCHAR(250))
UPDATE usuario SET
usu_contrasena=CONTRA
WHERE usu_id=IDUSUARIO$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_DATOS_USUARIO`(IN `IDUSUARIO` INT, IN `SEXO` CHAR(1), IN `IDROL` INT, IN `EMAIL` VARCHAR(250))
UPDATE usuario SET
usu_sexo=SEXO,
rol_id=IDROL,
usu_email=EMAIL
WHERE usu_id=IDUSUARIO$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_DEPARTAMENTO`(IN `ID` INT, IN `DEPARTAMENTOACTUAL` VARCHAR(50), IN `DEPARTAMENTONUEVO` VARCHAR(50), IN `GERENCIA` INT)
BEGIN
	DECLARE CANTIDAD INT;
IF DEPARTAMENTOACTUAL= DEPARTAMENTONUEVO THEN
	UPDATE departamento SET
	gerencia_id=GERENCIA
	where departamento_id= ID;
	SELECT 1;

ELSE

	SET @CANTIDAD:=(select count(*) from departamento where departamento_direccion=DEPARTAMENTONUEVO);
	IF @CANTIDAD = 0 THEN
	UPDATE departamento SET
	gerencia_id=GERENCIA,
	departamento_direccion=DEPARTAMENTONUEVO
	where departamento_id= ID;
	SELECT 1;
	ELSE
	SELECT 2;
	END IF;
END IF;	

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_ESTACION`(IN `ID` INT, IN `ESTACIONACTUAL` VARCHAR(50), IN `ESTACIONNUEVA` VARCHAR(50))
BEGIN
DECLARE CANTIDAD INT;
IF ESTACIONACTUAL=ESTACIONNUEVA THEN
	UPDATE estacion SET
	estacion_estatus=ESTATUS
	WHERE estacion_id=ID;
	SELECT 1;
ELSE
	SET @CANTIDAD:=(SELECT COUNT(*) FROM estacion WHERE estacion_nombre=ESTACIONNUEVA);
	IF @CANTIDAD=0 THEN
	UPDATE estacion SET
	estacion_nombre=ESTACIONNUEVA
	WHERE estacion_id=ID;
	SELECT 1;
	ELSE
	SELECT 2;
	END IF;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_ESTATUS_USUARIO`(IN `IDUSUARIO` INT, IN `ESTATUS` VARCHAR(20))
UPDATE usuario SET
usu_estatus=ESTATUS
where usu_id=IDUSUARIO$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_GERENCIA`(IN `ID` INT, IN `GERENCIAACTUAL` VARCHAR(50), IN `GERENCIANUEVA` VARCHAR(50), IN `ESTATUS` VARCHAR(10))
BEGIN
DECLARE CANTIDAD INT;
IF GERENCIAACTUAL=GERENCIANUEVA THEN
	UPDATE gerencia SET
	gerencia_estatus=ESTATUS
	WHERE gerencia_id=ID;
	SELECT 1;
ELSE
	SET @CANTIDAD:=(SELECT COUNT(*) FROM gerencia WHERE gerencia_nombre=GERENCIANUEVA);
	IF @CANTIDAD=0 THEN
	UPDATE gerencia SET
	gerencia_nombre=GERENCIANUEVA,
	gerencia_estatus=ESTATUS
	WHERE gerencia_id=ID;
	SELECT 1;
	ELSE
	SELECT 2;
	END IF;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_IMPRESORA`(IN `IDIMPRESORA` INT, IN `IDPROPIETARIO` INT, IN `IDMARCA` INT, IN `IDMODELO` INT, IN `ESTATUS` VARCHAR(20), IN `MACACTUAL` VARCHAR(20), IN `MACNUEVO` VARCHAR(20), IN `SERIEACTUAL` VARCHAR(50), IN `SERIENUEVO` VARCHAR(50), IN `IP` VARCHAR(20), IN `CONTACTO` VARCHAR(50), IN `INFO` VARCHAR(50), IN `ESTACION` INT, IN `GERENCIA` INT, IN `DEPARTAMENTO` INT)
BEGIN
DECLARE CANTIDAD INT;
IF SERIENUEVO=SERIEACTUAL THEN
	update impresora set
	propietario_id=IDPROPIETARIO,
	marca_id=IDMARCA,
	modelo_id=IDMODELO,
	impresora_estatus=ESTATUS,
	impresora_mac=MACNUEVO,
	impresora_serie=SERIENUEVO,
  impresora_ip=IP,
	impresora_contacto=CONTACTO,
	impresora_info=INFO,
	estacion_id=ESTACION,
  gerencia_id=GERENCIA,
	departamento_id=DEPARTAMENTO	
	WHERE impresora_id=IDIMPRESORA;
	SELECT 1;
ELSE
SET @CANTIDAD:=(SELECT COUNT(*) from impresora where impresora_serie = SERIENUEVO);
IF @CANTIDAD = 0 THEN
	update impresora set
	propietario_id=IDPROPIETARIO,
	marca_id=IDMARCA,
	modelo_id=IDMODELO,
	impresora_estatus=ESTATUS,
	impresora_mac=MACNUEVO,
	impresora_serie=SERIENUEVO,
	impresora_ip=IP,
	impresora_contacto=CONTACTO,
	impresora_info=INFO,
	estacion_id=ESTACION,
  gerencia_id=GERENCIA,
	departamento_id=DEPARTAMENTO
	WHERE impresora_id=IDIMPRESORA;
	select 1;
	ELSE
	SELECT 2;
	END IF;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_LSPERSONAL`(IN `IDLSPERSONAL` INT, IN `IDESTACION` INT, IN `IDGERENCIA` INT, IN `IDDEPARTAMENTO` INT, IN `ESTATUS` VARCHAR(20), IN `NOMBRE` VARCHAR(20), IN `INTERNO` VARCHAR(50))
BEGIN
	update lspersonal set
	estacion_id=IDESTACION,
	gerencia_id=IDGERENCIA,
	departamento_id=IDDEPARTAMENTO,
	lspersonal_estatus=ESTATUS,
	lspersonal_nombre=NOMBRE,
	lspersonal_interno=INTERNO	
	WHERE lspersonal_id=IDLSPERSONAL;
	SELECT 1;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_MARCA`(IN `ID` INT, IN `MARCAACTUAL` VARCHAR(50), IN `MARCANUEVA` VARCHAR(50), IN `ESTATUS` VARCHAR(10))
BEGIN
DECLARE CANTIDAD INT;
IF MARCAACTUAL=MARCANUEVA THEN
	UPDATE marca SET
	marca_estatus=ESTATUS
	WHERE marca_id=ID;
	SELECT 1;
ELSE
	SET @CANTIDAD:=(SELECT COUNT(*) FROM marca WHERE marca_nombre=MARCANUEVA);
	IF @CANTIDAD=0 THEN
	UPDATE marca SET
	marca_nombre=MARCANUEVA,
	marca_estatus=ESTATUS
	WHERE marca_id=ID;
	SELECT 1;
	ELSE
	SELECT 2;
	END IF;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_MODELO`(IN `ID` INT, IN `MODELOACTUAL` VARCHAR(50), IN `MODELONUEVO` VARCHAR(50), IN `MARCA` INT)
BEGIN
	DECLARE CANTIDAD INT;
IF MODELOACTUAL= MODELONUEVO THEN
	UPDATE modelo SET
	marca_id=MARCA
	where modelo_id= ID;
	SELECT 1;

ELSE

	SET @CANTIDAD:=(select count(*) from modelo where modelo_nombre=MODELONUEVO);
	IF @CANTIDAD = 0 THEN
	UPDATE modelo SET
	marca_id=MARCA,
	modelo_nombre=MODELONUEVO
	where modelo_id= ID;
	SELECT 1;
	ELSE
	SELECT 2;
	END IF;
END IF;	

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_MODIFICAR_PROPIETARIO`(IN `ID` INT, IN `PROPIETARIOACTUAL` VARCHAR(50), IN `PROPIETARIONUEVA` VARCHAR(50), IN `ESTATUS` VARCHAR(10))
BEGIN
DECLARE CANTIDAD INT;
IF PROPIETARIOACTUAL=PROPIETARIONUEVA THEN
	UPDATE propietario SET
	propietario_estatus=ESTATUS
	WHERE propietario_id=ID;
	SELECT 1;
ELSE
	SET @CANTIDAD:=(SELECT COUNT(*) FROM propietario WHERE propietario_nombre=PROPIETARIONUEVA);
	IF @CANTIDAD=0 THEN
	UPDATE propietario SET
	propietario_nombre=PROPIETARIONUEVA,
	propietario_estatus=ESTATUS
	WHERE propietario_id=ID;
	SELECT 1;
	ELSE
	SELECT 2;
	END IF;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REGISTRAR_DEPARTAMENTO`(IN `DEPARTAMENTO` VARCHAR(50), IN `GERENCIA` INT)
BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(SELECT COUNT(*) FROM departamento WHERE departamento_direccion=DEPARTAMENTO);

IF @CANTIDAD = 0 THEN
INSERT INTO departamento(departamento_direccion,gerencia_id)
VALUES(DEPARTAMENTO,GERENCIA);
SELECT 1;
ELSE
SELECT 2;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REGISTRAR_ESTACION`(IN `ESTACION` VARCHAR(50))
BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(select count(*) from estacion WHERE estacion_nombre=ESTACION);

IF @CANTIDAD = 0 THEN 
	INSERT INTO estacion(estacion_nombre) VALUES
	(ESTACION);
	SELECT 1;
ELSE
	SELECT 2;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REGISTRAR_GERENCIA`(IN `GERENCIA` VARCHAR(50), IN `ESTATUS` VARCHAR(10))
BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(select count(*) from gerencia WHERE gerencia_nombre=GERENCIA);

IF @CANTIDAD = 0 THEN 
	INSERT INTO gerencia(gerencia_nombre,gerencia_fregistro,gerencia_estatus) VALUES
	(GERENCIA,CURDATE(),ESTATUS);
	SELECT 1;
ELSE
	SELECT 2;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REGISTRAR_IMPRESORA`(IN `IDPROPIETARIO` INT, IN `IDMARCA` INT, IN `IDMODELO` INT, IN `MAC` VARCHAR(20), IN `SERIE` VARCHAR(50), IN `DIRECCIONIP` VARCHAR(30), IN `CONTACTO` VARCHAR(200), IN `INFO` VARCHAR(50), IN `IDESTACION` INT, IN `IDGERENCIA` INT, IN `IDDEPARTAMENTO` INT, IN `ESTATUS` VARCHAR(20))
BEGIN
DECLARE CANTIDADIMPRESORA INT;
SET @CANTIDADIMPRESORA:=(SELECT COUNT(*) FROM impresora WHERE impresora_mac=MAC OR impresora_serie=SERIE);
	IF @CANTIDADIMPRESORA = 0 THEN
	INSERT INTO impresora(propietario_id,marca_id,modelo_id,impresora_mac,impresora_serie,impresora_ip,impresora_contacto,impresora_info,estacion_id,gerencia_id,departamento_id,impresora_feregistro,impresora_estatus)
	VALUES(IDPROPIETARIO,IDMARCA,IDMODELO,MAC,SERIE,DIRECCIONIP,CONTACTO,INFO,IDESTACION,IDGERENCIA,IDDEPARTAMENTO,CURDATE(),ESTATUS);
	SELECT 1;	
ELSE
	SELECT 2;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REGISTRAR_LSPERSONAL`(IN `IDESTACION` INT, IN `IDGERENCIA` INT, IN `IDDEPARTAMENTO` INT, IN `NOMBRE` VARCHAR(20), IN `INTERNO` VARCHAR(50))
BEGIN
DECLARE CANTIDADLSPERSONAL INT;
SET @CANTIDADLSPERSONAL:=(SELECT COUNT(*) FROM lspersonal WHERE lspersonal_nombre=NOMBRE );
	IF @CANTIDADLSPERSONAL = 0 THEN
	INSERT INTO lspersonal(estacion_id,gerencia_id,departamento_id,lspersonal_nombre,lspersonal_interno,lspersonal_feregistro,lspersonal_estatus)
	VALUES(IDESTACION,IDGERENCIA,IDDEPARTAMENTO,NOMBRE,INTERNO,CURDATE(),'ACTIVO');
	SELECT 1;	
ELSE
	SELECT 2;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REGISTRAR_MARCA`(IN `MARCA` VARCHAR(50), IN `ESTATUS` VARCHAR(10))
BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(select count(*) from marca WHERE marca_nombre=MARCA);

IF @CANTIDAD = 0 THEN 
	INSERT INTO marca(marca_nombre,marca_fregistro,marca_estatus) VALUES
	(MARCA,CURDATE(),ESTATUS);
	SELECT 1;
ELSE
	SELECT 2;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REGISTRAR_MODELO`(IN `MODELO` VARCHAR(50), IN `MARCA` INT)
BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(SELECT COUNT(*) FROM modelo WHERE modelo_nombre=MODELO);

IF @CANTIDAD = 0 THEN
INSERT INTO modelo(modelo_nombre,marca_id)
VALUES(MODELO,MARCA);
SELECT 1;
ELSE
SELECT 2;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REGISTRAR_PROPIETARIO`(IN `PROPIETARIO` VARCHAR(50), IN `ESTATUS` VARCHAR(10))
BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(select count(*) from propietario WHERE propietario_nombre=PROPIETARIO);

IF @CANTIDAD = 0 THEN 
	INSERT INTO propietario(propietario_nombre,propietario_fregistro,propietario_estatus) VALUES
	(PROPIETARIO,CURDATE(),ESTATUS);
	SELECT 1;
ELSE
	SELECT 2;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_REGISTRAR_USUARIO`(IN `USU` VARCHAR(20), IN `CONTRA` VARCHAR(250), IN `SEXO` CHAR(1), IN `ROL` INT, IN `EMAIL` VARCHAR(250))
BEGIN 
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(select count(*) from usuario where usu_nombre= BINARY USU);
IF @CANTIDAD=0 THEN
INSERT INTO usuario(usu_nombre,usu_contrasena,usu_sexo,rol_id,usu_estatus,usu_email,usu_intento) VALUES (USU,CONTRA,SEXO,ROL,'ACTIVO',EMAIL,0);
SELECT 1;
ELSE
SELECT 2;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_RESTABLECER_CONTRA`(IN `EMAIL` VARCHAR(255), IN `CONTRA` VARCHAR(255))
BEGIN 
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(select COUNT(*) from usuario where usu_email=EMAIL);
IF @CANTIDAD>0 THEN
	UPDATE usuario SET 
	usu_contrasena=CONTRA,
	usu_intento=0
	WHERE usu_email=EMAIL;
	select 1;
ELSE
	select 2;
END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_VERIFICAR_USUARIO`(IN `USUARIO` VARCHAR(20))
SELECT
usuario.usu_id,
usuario.usu_nombre,
usuario.usu_contrasena,
usuario.usu_sexo,
usuario.rol_id,
usuario.usu_estatus,
rol.rol_nombre,
usuario.usu_intento
FROM
usuario
INNER JOIN rol ON usuario.rol_id = rol.rol_id
WHERE usu_nombre= BINARY USUARIO$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `S_Navicat_Temp_Stored_Proc`(IN `IDIMPRESORA` INT, IN `IDPROPIETARIO` INT, IN `IDMARCA` INT, IN `IDMODELO` INT, IN `MACNUEVO` VARCHAR(20), IN `MACACTUAL` VARCHAR(20), IN `SERIENUEVO` VARCHAR(50), IN `SERIEACTUAL` VARCHAR(50), IN `ESTATUS` VARCHAR(20))
BEGIN
DECLARE CANTIDAD INT;
IF SERIEACTUAL=SERIENUEVO THEN
	update impresora set
	propietario_id=IDPROPIETARIO,
	marca_id=IDMARCA,
	modelo_id=IDMODELO,
	impresora_mac=MACNUEVO,
	impresora_serie=SERIENUEVO,
	impresora_estatus=ESTATUS
	WHERE impresora_id=IDIMPRESORA;
	SELECT 1;
ELSE
SET @CANTIDAD:=(SELECT COUNT(*) from impresora where impresora_serie = SERIENUEVO);
IF @CANTIDAD = 0 THEN
	update impresora set
	propietario_id=IDPROPIETARIO,
	marca_id=IDMARCA,
	modelo_id=IDMODELO,
	impresora_mac=MACNUEVO,
	impresora_serie=SERIENUEVO,
	impresora_estatus=ESTATUS
	WHERE impresora_id=IDIMPRESORA;
	select 1;
	ELSE
	SELECT 2;
	END IF;
	END IF;
END$$
DELIMITER ;

CREATE TABLE `departamento` (
  `departamento_id` int(11) NOT NULL,
  `departamento_direccion` varchar(50) DEFAULT NULL,
  `gerencia_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`departamento_id`, `departamento_direccion`, `gerencia_id`) VALUES
(1, 'Administracion', 1),
(2, 'trenes', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estacion`
--

CREATE TABLE `estacion` (
  `estacion_id` int(11) NOT NULL,
  `estacion_nombre` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estacion`
--

INSERT INTO `estacion` (`estacion_id`, `estacion_nombre`) VALUES
(1, 'Once'),
(2, 'Caballito'),
(3, 'Flores'),
(4, 'Floresta'),
(5, 'Liniers'),
(6, 'Villa Luro'),
(7, 'Ciudadela'),
(8, 'Ramos Mejia'),
(9, 'Haedo'),
(10, 'Moron'),
(11, 'Castelar'),
(12, 'Ituzaingo'),
(13, 'SA Padua'),
(14, 'Merlo'),
(15, 'Paso del rey'),
(16, 'Moreno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gerencia`
--

CREATE TABLE `gerencia` (
  `gerencia_id` int(11) NOT NULL,
  `gerencia_nombre` varchar(50) DEFAULT NULL,
  `gerencia_fregistro` date DEFAULT NULL,
  `gerencia_estatus` enum('ACTIVO','INACTIVO') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gerencia`
--

INSERT INTO `gerencia` (`gerencia_id`, `gerencia_nombre`, `gerencia_fregistro`, `gerencia_estatus`) VALUES
(1, 'Material Rodante', '2020-11-22', 'ACTIVO'),
(2, 'Transporte', '2020-11-22', 'ACTIVO'),
(3, 'Infraestructura', '2020-11-22', 'ACTIVO'),
(4, 'RRHH', '2020-11-22', 'ACTIVO'),
(5, 'GTI', '2020-11-22', 'ACTIVO'),
(6, 'AACC', '2020-11-22', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impresora`
--

CREATE TABLE `impresora` (
  `impresora_id` int(11) NOT NULL,
  `impresora_feregistro` date DEFAULT NULL,
  `modelo_id` int(11) DEFAULT NULL,
  `marca_id` int(11) DEFAULT NULL,
  `propietario_id` int(11) DEFAULT NULL,
  `impresora_estatus` enum('LIBRE','REPARAR','INSTALADA')  DEFAULT NULL,
  `impresora_mac` varchar(20)  DEFAULT NULL,
  `impresora_serie` varchar(50)  DEFAULT NULL,
  `impresora_ip` varchar(20)  DEFAULT NULL,
  `estacion_id` int(11) DEFAULT NULL,
  `gerencia_id` int(11) DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  `impresora_contacto` varchar(255)  DEFAULT NULL,
  `impresora_info` varchar(255)  DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `impresora`
--

INSERT INTO `impresora` (`impresora_id`, `impresora_feregistro`, `modelo_id`, `marca_id`, `propietario_id`, `impresora_estatus`, `impresora_mac`, `impresora_serie`, `impresora_ip`, `estacion_id`, `gerencia_id`, `departamento_id`, `impresora_contacto`, `impresora_info`) VALUES
(11, '2020-11-23', 1, 1, 6, 'INSTALADA', 'aa:aa:pp', 'nnknska', '172.20.10.20', 1, 1, 1, 'asodiupo123', 'isdlkcnsld'),
(12, '2020-11-23', 1, 1, 6, 'REPARAR', 'mimac', 'miserie', '172.20.40.40', 1, 1, 1, '14151', 'Infoalgo'),
(13, '2020-11-23', 4, 2, 6, 'LIBRE', 'jufb', 'bwdydcdf', '10.50.114.20', 4, 2, 2, 'Jonathan 13740', 'Solo miercoles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lspersonal`
--

CREATE TABLE `lspersonal` (
  `lspersonal_id` int(11) NOT NULL,
  `lspersonal_nombre` varchar(255) DEFAULT NULL,
  `lspersonal_feregistro` varchar(255) DEFAULT NULL,
  `estacion_id` int(11) DEFAULT NULL,
  `gerencia_id` int(11) DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  `lspersonal_estatus` enum('ACTIVO','INACTIVO') DEFAULT NULL,
  `lspersonal_interno` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lspersonal`
--

INSERT INTO `lspersonal` (`lspersonal_id`, `lspersonal_nombre`, `lspersonal_feregistro`, `estacion_id`, `gerencia_id`, `departamento_id`, `lspersonal_estatus`, `lspersonal_interno`) VALUES
(1, 'Jonathan Santucho', '2020-11-22', 1, 1, 1, 'ACTIVO', '13740');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `marca_id` int(11) NOT NULL,
  `marca_nombre` varchar(50) DEFAULT NULL,
  `marca_fregistro` date DEFAULT NULL,
  `marca_estatus` enum('ACTIVO','INACTIVO') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`marca_id`, `marca_nombre`, `marca_fregistro`, `marca_estatus`) VALUES
(1, 'HP', '2020-11-22', 'ACTIVO'),
(2, 'SAMSUNG', '2020-11-22', 'ACTIVO'),
(3, 'LEXMARK', '2020-11-22', 'ACTIVO'),
(4, 'BROTHER', '2020-11-22', 'ACTIVO'),
(5, 'XEROX', '2020-11-22', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelo`
--

CREATE TABLE `modelo` (
  `modelo_id` int(11) NOT NULL,
  `modelo_nombre` varchar(50) DEFAULT NULL,
  `marca_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modelo`
--

INSERT INTO `modelo` (`modelo_id`, `modelo_nombre`, `marca_id`) VALUES
(1, '426', 1),
(2, '428', 1),
(3, '3225', 5),
(4, '310', 2),
(5, '3220', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `propietario`
--

CREATE TABLE `propietario` (
  `propietario_id` int(11) NOT NULL,
  `propietario_nombre` varchar(50) DEFAULT NULL,
  `propietario_fregistro` date DEFAULT NULL,
  `propietario_estatus` enum('ACTIVO','INACTIVO') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `propietario`
--

INSERT INTO `propietario` (`propietario_id`, `propietario_nombre`, `propietario_fregistro`, `propietario_estatus`) VALUES
(6, 'LS', '2020-11-22', 'ACTIVO'),
(7, 'IT GROUP', '2020-11-22', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `rol_id` int(11) NOT NULL,
  `rol_nombre` varchar(30) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`rol_id`, `rol_nombre`) VALUES
(1, 'ADMINISTRADOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usu_id` int(11) NOT NULL,
  `usu_nombre` varchar(20) DEFAULT NULL,
  `usu_contrasena` varchar(255) DEFAULT NULL,
  `usu_sexo` char(1) DEFAULT NULL,
  `rol_id` int(11) DEFAULT NULL,
  `usu_estatus` enum('ACTIVO','INACTIVO') DEFAULT NULL,
  `usu_email` varchar(255) DEFAULT NULL,
  `usu_intento` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usu_id`, `usu_nombre`, `usu_contrasena`, `usu_sexo`, `rol_id`, `usu_estatus`, `usu_email`, `usu_intento`) VALUES
(7, 'admin', '$2y$10$mwfHEHJKFB8Xjtqaa17x3ejH6DPI28YIIzfvzY460gPzQSXtQ.Aba', 'M', 1, 'ACTIVO', 'santuchojd@gmail.com', 1),
(11, 'prueba', '$2y$10$gUQpIpmDoHY9kd2e2oJKwOj/32SD6cPh12fGhAdnW11ZkXwpGCeI2', 'M', 1, 'ACTIVO', 'jonathan.santucho@trenesargentinos.gob.ar', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`departamento_id`) USING BTREE,
  ADD KEY `gerencia_id` (`gerencia_id`);

--
-- Indices de la tabla `estacion`
--
ALTER TABLE `estacion`
  ADD PRIMARY KEY (`estacion_id`);

--
-- Indices de la tabla `gerencia`
--
ALTER TABLE `gerencia`
  ADD PRIMARY KEY (`gerencia_id`) USING BTREE;

--
-- Indices de la tabla `impresora`
--
ALTER TABLE `impresora`
  ADD PRIMARY KEY (`impresora_id`) USING BTREE,
  ADD KEY `impresora_ibfk_1` (`modelo_id`),
  ADD KEY `impresora_ibfk_2` (`marca_id`),
  ADD KEY `propietario_id` (`propietario_id`),
  ADD KEY `estacion_id` (`estacion_id`),
  ADD KEY `gerencia_id` (`gerencia_id`),
  ADD KEY `departamento_id` (`departamento_id`);

--
-- Indices de la tabla `lspersonal`
--
ALTER TABLE `lspersonal`
  ADD PRIMARY KEY (`lspersonal_id`),
  ADD KEY `estacion_id` (`estacion_id`),
  ADD KEY `gerencia_id` (`gerencia_id`),
  ADD KEY `departamento_id` (`departamento_id`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`marca_id`) USING BTREE;

--
-- Indices de la tabla `modelo`
--
ALTER TABLE `modelo`
  ADD PRIMARY KEY (`modelo_id`) USING BTREE,
  ADD KEY `marca_id` (`marca_id`);

--
-- Indices de la tabla `propietario`
--
ALTER TABLE `propietario`
  ADD PRIMARY KEY (`propietario_id`) USING BTREE;

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`rol_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usu_id`),
  ADD KEY `rol_id` (`rol_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `departamento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `estacion`
--
ALTER TABLE `estacion`
  MODIFY `estacion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `gerencia`
--
ALTER TABLE `gerencia`
  MODIFY `gerencia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `impresora`
--
ALTER TABLE `impresora`
  MODIFY `impresora_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `lspersonal`
--
ALTER TABLE `lspersonal`
  MODIFY `lspersonal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `marca_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `modelo`
--
ALTER TABLE `modelo`
  MODIFY `modelo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `propietario`
--
ALTER TABLE `propietario`
  MODIFY `propietario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `rol_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD CONSTRAINT `departamento_ibfk_1` FOREIGN KEY (`gerencia_id`) REFERENCES `gerencia` (`gerencia_id`);

--
-- Filtros para la tabla `impresora`
--
ALTER TABLE `impresora`
  ADD CONSTRAINT `impresora_ibfk_1` FOREIGN KEY (`modelo_id`) REFERENCES `modelo` (`modelo_id`),
  ADD CONSTRAINT `impresora_ibfk_2` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`marca_id`),
  ADD CONSTRAINT `impresora_ibfk_3` FOREIGN KEY (`propietario_id`) REFERENCES `propietario` (`propietario_id`),
  ADD CONSTRAINT `impresora_ibfk_4` FOREIGN KEY (`estacion_id`) REFERENCES `estacion` (`estacion_id`),
  ADD CONSTRAINT `impresora_ibfk_5` FOREIGN KEY (`gerencia_id`) REFERENCES `gerencia` (`gerencia_id`),
  ADD CONSTRAINT `impresora_ibfk_6` FOREIGN KEY (`departamento_id`) REFERENCES `departamento` (`departamento_id`);

--
-- Filtros para la tabla `lspersonal`
--
ALTER TABLE `lspersonal`
  ADD CONSTRAINT `lspersonal_ibfk_1` FOREIGN KEY (`estacion_id`) REFERENCES `estacion` (`estacion_id`),
  ADD CONSTRAINT `lspersonal_ibfk_2` FOREIGN KEY (`gerencia_id`) REFERENCES `gerencia` (`gerencia_id`),
  ADD CONSTRAINT `lspersonal_ibfk_3` FOREIGN KEY (`departamento_id`) REFERENCES `departamento` (`departamento_id`);

--
-- Filtros para la tabla `modelo`
--
ALTER TABLE `modelo`
  ADD CONSTRAINT `modelo_ibfk_1` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`marca_id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`rol_id`);

  
