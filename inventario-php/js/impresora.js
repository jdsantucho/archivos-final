var tableimpresora;
function listar_impresora(){
        var printCounter = 0;
        var now = new Date();
        var fecha = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear(); 
        tableimpresora = $("#tabla_impresora").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copy',
                text : '<i class="fa fa-clipboard"> Copiar tabla</i>',
                exportOptions: {
                    columns: [ 1, 2, 3,4, 5, 6, 7,8,9,10 ]
                }
            },
            {
                extend: 'excel',
                messageTop: 'Listado de impresoras al '+fecha+ ' | Linea Sarmiento',
                messageBottom: 'Fin del documento',
                text : '<i class="fa fa-file-excel-o"> Excel</i>',
                exportOptions: {
                    columns: [ 1, 2, 3,4, 5, 6, 7,8,9,10 ]
                }
            },
            {
      extend: 'pdfHtml5',
      text: ' <i class="fa fa-file-pdf-o"> PDF</i>',
      exportOptions: {
         modifier: {
            page: 'current'
         }
      },
      title:'Linea Sarmiento',
      header: true,
      orientation: 'portrait',
      messageTop: 'Listado de impresoras al '+fecha+ ' | Linea Sarmiento',
      messageBottom: 'Fin del listado',
      exportOptions: {
                    columns: [ 1, 2, 3,4, 5, 6, 7,8,9,10 ]
                },
      customize: function(doc) {
        doc.defaultStyle.alignment = 'center';
        doc.defaultStyle.fontSize = 8;
        doc.styles.tableHeader.fontSize = 10;
                        
        var now = new Date();
        var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear()+'    '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds(); 
        doc['footer']=(function(page, pages) {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        text: ['Fecha: ', { text: jsDate.toString() }]
                                    },
                                    {
                                        alignment: 'right',
                                        text: ['Pagina ', { text: page.toString() },  ' de ', { text: pages.toString() }]
                                    }
                                ],
                                margin: 20
                            }
                        });
     }  
}, 
            {
                extend: 'print',
                text : '<i class="fa fa-print"> Imprimir</i>',
                messageTop: function () {
                    printCounter++;
 
                    if ( printCounter === 1 ) {
                        return 'Listado de impresoras al '+fecha+ ' | Linea Sarmiento';
                    }
                    else {
                        return 'Este documento se imprimió '+printCounter+' veces';
                    }
                },
                messageBottom: 'Listado de impresoras al '+fecha+ ' | Linea Sarmiento',
                exportOptions: {
                    columns: [ 1, 2, 3,4, 5, 6, 7,8,9,10 ]
                }
            }
        ],
        "order":[[1, 'asc']],
        "ordering":true,
        "bLengthChange":false,
        "paging": true,
        "searching": { "regex": true },
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "pageLength": 10,
        "destroy":true,
        "async": false ,
        "processing": true,
        "ajax":{
            "url":"../controlador/impresora/controlador_impresora_listar.php",
            type:'POST'
        },
        "order":[[1, 'asc']],
        "columns":[
            {"defaultContent":""},
            {"data":"impresora_ip"},
            {"data":"modelo"},
            {"data":"departamento"},
            {"data":"impresora_contacto"},
            {"data":"estacion"},
            {"data":"impresora_serie"},
            {"defaultContent":"<button style='font-size:13px;' type='button' class='editar btn btn-primary'><i class='fa fa-edit'></i></button>&nbsp;<button style='font-size:13px;' type='button' class='detalles btn btn-white' title='detalles'><i class='fa fa-eye'></i></button>"},
            {"data":"impresora_estatus",
            render: function (data, type, row ) {
                if(data=='INSTALADA'){
                    return "<span class='label label-success'>"+data+"</span>";                   
                }if(data=='REPARAR'){
                    return "<span class='label label-danger'>"+data+"</span>";                   
                }if(data=='LIBRE'){
                    return "<span class='label label-primary' style='background:black'>"+data+"</span>";                   
                }
              }
            },
            {"data":"impresora_mac"},
            {"data":"propietario",
            render: function (data, type, row ) {
                if(data=='LS'){
                    return "<span class='label label-primary'>"+data+"</span>";                   
                }else{
                    return "<span class='label label-warning'>"+data+"</span>";                   
                }
              }
            }
            
        ],

        "language":idioma_espanol,
        "columnDefs": [
                {className: "dt-center", "targets": [1, 2, 3,4, 5, 6, 7,8,9]
                }
            ],
        select: true
    });
    document.getElementById("tabla_impresora_filter").style.display="none";
    $('input.global_filter').on('keyup click',function(){
        filterGlobal();
    });
    $('input.column_filter').on('keyup click',function(){
        filterColumn($(this).parents('tr').attr('data-column'));
    });

    tableimpresora.on('draw.dt', function(){
        var PageInfo = $('#tabla_impresora').DataTable().page.info();
        tableimpresora.column(0, { page: 'current'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

}

function filterGlobal(){
    $('#tabla_impresora').DataTable().search(
        $('#global_filter').val(),
    ).draw();
}

$('#tabla_impresora').on('click','.editar',function(){
    var data = tableimpresora.row($(this).parents('tr')).data();//Detecta a que fila hago click y me trae los datos en la variable data
    if(tableimpresora.row(this).child.isShown()){
        var data = tableimpresora.row(this).data();
    }
    $("#modal_editar").modal({backdrop:'static',keyboard:false})
    $("#modal_editar").modal('show');
    $("#txt_impresora_id").val(data.impresora_id);
    $("#cbm_propietario_editar").val(data.propietario_id).trigger("change");
    $("#cbm_marca_editar").val(data.marca_id).trigger("change");
    listar_modelo_combo_editar(data.marca_id,data.modelo_id);
    $("#cbm_gerencia_editar").val(data.gerencia_id).trigger("change");
    listar_departamento_combo_editar(data.gerencia_id,data.departamento_id);
    $("#cbm_estatus_editar").val(data.impresora_estatus).trigger("change");
    $("#txt_mac_actual_editar").val(data.impresora_mac);
    $("#txt_mac_nuevo_editar").val(data.impresora_mac);
    $("#txt_serie_actual_editar").val(data.impresora_serie);
    $("#txt_serie_nuevo_editar").val(data.impresora_serie);
    $("#txt_direccionip_editar").val(data.impresora_ip);
    $("#txt_contacto_editar").val(data.impresora_contacto);
    $("#txt_info_editar").val(data.impresora_info);
    $("#cbm_estacion_editar").val(data.estacion_id).trigger("change");

})

$('#tabla_impresora').on('click','.detalles',function(){
    var data = tableimpresora.row($(this).parents('tr')).data();//Detecta a que fila hago click y me trae los datos en la variable data
    if(tableimpresora.row(this).child.isShown()){
        var data = tableimpresora.row(this).data();
    }
    $("#modal_detalles").modal({backdrop:'static',keyboard:false})
    $("#modal_detalles").modal('show');
    $("#txt_impresora_id").val(data.impresora_id);
    $("#cbm_propietario_detalle").val(data.propietario_id).trigger("change");
    $("#cbm_marca_detalle").val(data.marca_id).trigger("change");
    listar_modelo_combo_editar(data.marca_id,data.modelo_id);
    $("#cbm_gerencia_detalle").val(data.gerencia_id).trigger("change");
    listar_departamento_combo_editar(data.gerencia_id,data.departamento_id);
    $("#cbm_estatus_detalle").val(data.impresora_estatus).trigger("change");
    $("#txt_mac_actual_detalle").val(data.impresora_mac);
    $("#txt_mac_nuevo_detalle").val(data.impresora_mac);
    $("#txt_serie_actual_detalle").val(data.impresora_serie);
    $("#txt_serie_nuevo_detalle").val(data.impresora_serie);
    $("#txt_direccionip_detalle").val(data.impresora_ip);
    $("#txt_contacto_detalle").val(data.impresora_contacto);
    $("#txt_info_detalle").val(data.impresora_info);
    $("#cbm_estacion_detalle").val(data.estacion_id).trigger("change");

})

$('#tabla_impresora').on('click','.imprimir',function(){
    var data = tableimpresora.row($(this).parents('tr')).data();//Detecta a que fila hago click y me trae los datos en la variable data
    if(tableimpresora.row(this).child.isShown()){
        var data = tableimpresora.row(this).data();
    }
    window.open("../vista/libreporte/reportes/generar_ticket.php?id="+parseInt(data.impresora_id)+"#zoom=100%","Ticket","scrollbars=NO");
})


function Registrar_Impresora(){
    var idpropietario = $("#cbm_propietario").val();
    var idmarca = $("#cbm_marca").val();
    var idmodelo = $("#cbm_modelo").val();
    var direccionmac = $("#txt_direccionmac").val();
    var numeroserie = $("#txt_numeroserie").val();
    var direccionip = $("#txt_direccionip").val();
    var contacto = $("#txt_contacto").val();
    var info = $("#txt_info").val();
    var idestacion = $("#cbm_estacion").val();
    var idgerencia = $("#cbm_gerencia").val();
    var iddepartamento = $("#cbm_departamento").val();
    var estatus = $("#cbm_estatus").val();
    //alert(idpropietario+" - "+iddepartamento+" - "+descripcion+" - "+idmarca+" - "+idusuario);
    if(idpropietario.length==0 || idmarca.length==0 || idmodelo.length==0 || direccionmac.length==0 || numeroserie.length==0 || direccionip.length==0 || contacto.length==0 || idestacion.length==0 || idgerencia.length==0 || iddepartamento.length==0 || estatus.length==0){
        return Swal.fire("Mensaje De Advertencia", "Llene los campos vacios!", "warning");
    }

    $.ajax({
        "url": "../controlador/impresora/controlador_impresora_registro.php",
        type:'POST',
        data:{
            idpropietario:idpropietario,
            idmarca:idmarca,
            idmodelo:idmodelo,
            direccionmac:direccionmac,
            numeroserie:numeroserie,
            direccionip:direccionip,
            contacto:contacto,
            info:info,
            idestacion:idestacion,
            idgerencia:idgerencia,
            iddepartamento:iddepartamento,
            estatus:estatus
        }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                listar_impresora();
                LimpiarCampos();
                $("#modal_registro").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Impresora registrada", "success");
                }else{
                Swal.fire("Mensaje de advertencia", "El numero de serie o mac ya existe en la base de datos", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}

function Editar_Impresora(){
    var idimpresora = $("#txt_impresora_id").val();
    var idpropietario = $("#cbm_propietario_editar").val();
    var idmarca = $("#cbm_marca_editar").val();
    var idmodelo = $("#cbm_modelo_editar").val();
    var estatus = $("#cbm_estatus_editar").val();
    var macactual = $("#txt_mac_actual_editar").val();
    var macnuevo = $("#txt_mac_nuevo_editar").val();
    var serieactual = $("#txt_serie_actual_editar").val();
    var serienuevo = $("#txt_serie_nuevo_editar").val();
    var ip = $("#txt_direccionip_editar").val();
    var contacto = $("#txt_contacto_editar").val();
    var info = $("#txt_info_editar").val();
    var estacion = $("#cbm_estacion_editar").val();
    var gerencia = $("#cbm_gerencia_editar").val();
    var departamento = $("#cbm_departamento_editar").val();

    //alert(idimpresora+" - "+idpropietario+" - "+idmarca+" - "+idmodelo+" - "+idmarca+" - "+estatus+" - "+macactual+" - "+macnuevo+" - "+serieactual+" - "+serienuevo);
    if (idimpresora.length == 0 || idpropietario.length == 0 || idmarca.length == 0 || idmodelo.length == 0 || estatus.length == 0 || macactual.length == 0 || macnuevo.length == 0 || serieactual.length == 0 || serienuevo.length == 0 || ip.length == 0 || contacto.length == 0 || estacion.length == 0 || gerencia.length == 0 || departamento.length == 0) {
        return Swal.fire("Mensaje De Advertencia", "Llene los campos vacios!", "warning");
    }

    $.ajax({
        "url": "../controlador/impresora/controlador_impresora_editar.php",
        type:'POST',
        data:{
            idimpresora:idimpresora,
            idpropietario:idpropietario,
            idmarca:idmarca,
            idmodelo:idmodelo,
            estatus:estatus,
            macactual:macactual,
            macnuevo:macnuevo,
            serieactual:serieactual,
            serienuevo:serienuevo,
            ip:ip,
            contacto:contacto,
            info:info,
            estacion:estacion,
            gerencia:gerencia,
            departamento:departamento
        }
    }).done(function(resp){
         //alert(resp);
         if(resp>0){
               if(resp==1){
                listar_impresora();
                LimpiarCampos();
                $("#modal_editar").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Datos actualizados correctamente", "success");
                }else{
                Swal.fire("Mensaje de advertencia", "La impresora ya existe en la base de datos", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "La modificacion no se pudo completar", "error");
        }
    })
}


function listar_propietario_combo(){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_propietario_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_propietario").html(cadena);
            $("#cbm_propietario_editar").html(cadena);
            
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_propietario").html(cadena);
            $("#cbm_propietario_editar").html(cadena);
            
        }
    })
}

function listar_propietario_combo_editar(){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_propietario_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_propietario_editar").html(cadena);
            $("#cbm_propietario_detalle").html(cadena);
           
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_propietario_editar").html(cadena);
            $("#cbm_propietario_detalle").html(cadena);
            
        }
    })
}

function listar_marca_combo(){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_marca_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_marca").html(cadena);
            var id= $("#cbm_marca").val();
            listar_modelo_combo(id);
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_marca").html(cadena);

            
        }
    })
}

function listar_marca_combo_editar(){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_marca_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_marca_editar").html(cadena);
            $("#cbm_marca_detalle").html(cadena);
            var id= $("#cbm_marca_editar").val();
            listar_modelo_combo_editar(id);
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_marca_editar").html(cadena);


            
        }
    })
}

function listar_modelo_combo(id){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_modelo_listar.php",
        type:'POST',
        data:{
            id:id
        }
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_modelo").html(cadena);
           
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_modelo").html(cadena);

            
        }
    })
}

function listar_modelo_combo_editar(id,idmarca){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_modelo_listar.php",
        type:'POST',
        data:{
            id:id
        }
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_modelo_detalle").html(cadena);
            $("#cbm_modelo_editar").html(cadena);
            if(idmarca!=''){
               $("#cbm_modelo_editar").val(idmarca).trigger("change"); 
            }
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_modelo_editar").html(cadena);

            
        }
    })
}



function LimpiarCampos(){
    $("#txt_direccionmac").val("");
    $("#txt_numeroserie").val("");
    $("#txt_direccionip").val("");
    $("#txt_contacto").val("");
    $("#txt_info").val("");
    $("#cbm_propietario").val("");
    $("#cbm_marca").val("");
    $("#cbm_modelo").val("");
    $("#cbm_estatus").val("");

}

function listar_estacion_combo(){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_estacion_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_estacion").html(cadena);
            $("#cbm_estacion_editar").html(cadena);
            
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_estacion").html(cadena);
            $("#cbm_estacion_editar").html(cadena);
            
        }
    })
}

function listar_estacion_combo_editar(){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_estacion_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_estacion_editar").html(cadena);
            $("#cbm_estacion_detalle").html(cadena);
           
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_estacion_editar").html(cadena);
            $("#cbm_estacion_detalle").html(cadena);
            
        }
    })
}

function listar_gerencia_combo(){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_gerencia_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_gerencia").html(cadena);
            var id= $("#cbm_gerencia").val();
            listar_departamento_combo(id);
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_gerencia").html(cadena);

            
        }
    })
}

function listar_gerencia_combo_editar(){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_gerencia_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_gerencia_detalle").html(cadena);
            $("#cbm_gerencia_editar").html(cadena);
            var id= $("#cbm_gerencia_editar").val();
            listar_departamento_combo_editar(id);
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_gerencia_editar").html(cadena);

            
        }
    })
}

function listar_departamento_combo(id){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_departamento_listar.php",
        type:'POST',
        data:{
            id:id
        }
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_departamento").html(cadena);
           
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_departamento").html(cadena);

            
        }
    })
}

function listar_departamento_combo_editar(id,idgerencia){
    $.ajax({
        "url":"../controlador/impresora/controlador_combo_departamento_listar.php",
        type:'POST',
        data:{
            id:id
        }
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_departamento_detalle").html(cadena);
            $("#cbm_departamento_editar").html(cadena);
            if(idgerencia!=''){
               $("#cbm_departamento_editar").val(idgerencia).trigger("change"); 
            }
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_departamento_editar").html(cadena);

            
        }
    })
}

