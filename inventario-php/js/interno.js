var tableinterno;
function listar_interno(){
        tableinterno = $("#tabla_interno").DataTable({
        "ordering":true,
        "bLengthChange":false,
        "paging": true,
        "searching": { "regex": true },
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "pageLength": 10,
        "destroy":true,
        "async": false ,
        "processing": true,
        "ajax":{
            "url":"../controlador/interno/controlador_interno_listar.php",
            type:'POST'
        },
        "order":[[1, 'asc']],
        "columns":[
            {"defaultContent":""},
            {"data":"interno_nombre"},
            {"data":"interno_numero"},
            {"defaultContent":"<button style='font-size:13px;' type='button' class='editar btn btn-primary'><i class='fa fa-edit'></i></button>"}
        ],

        "language":idioma_espanol,
        "columnDefs": [
                {className: "dt-center", "targets": [0,1,2,3]
                }
            ],

        select: true
    });
    document.getElementById("tabla_interno_filter").style.display="none";
    $('input.global_filter').on('keyup click',function(){
        filterGlobal();
    });
    $('input.column_filter').on('keyup click',function(){
        filterColumn($(this).parents('tr').attr('data-column'));
    });

    tableinterno.on('draw.dt', function(){
        var PageInfo = $('#tabla_interno').DataTable().page.info();
        tableinterno.column(0, { page: 'current'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

}

function filterGlobal(){
    $('#tabla_interno').DataTable().search(
        $('#global_filter').val(),
    ).draw();
}

function Registrar_Interno(){
    var interno = $("#txt_interno").val();
    var telefono = $("#txt_numinterno").val();

    if(interno.length==0 || telefono.length==0){
        return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning");
    }

    $.ajax({
        "url":"../controlador/interno/controlador_interno_registro.php",
         type:'POST',
         data:{
            interno:interno,
            telefono:telefono
         }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                listar_interno();
                LimpiarCampos();
                $("#modal_registro").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Interno registrada", "success");
                }else{
                LimpiarCampos();
                Swal.fire("Mensaje de advertencia", "La interno ya existe en la base de datos", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}

function LimpiarCampos(){
    $("#txt_interno").val("");
    $("#txt_numinterno").val("");
}

$('#tabla_interno').on('click','.editar',function(){
    var data = tableinterno.row($(this).parents('tr')).data();//Detecta a que fila hago click y captura los datos en la variable data
    if(tableinterno.row(this).child.isShown()){//Cuando esta en tamaño responsivo
        var data = tableinterno.row(this).data();
    }
    $("#modal_editar").modal({backdrop:'static',keyboard:false});
    $("#modal_editar").modal('show');
    $("#id_interno").val(data.interno_id);
    $("#txt_interno_actual_editar").val(data.interno_nombre);
    $("#txt_interno_nueva_editar").val(data.interno_nombre);
    $("#txt_numinterno_editar").val(data.interno_numero);
})

function Editar_Interno(){
    var id = $("#id_interno").val();
    var internoactual = $("#txt_interno_actual_editar").val();
    var internonueva = $("#txt_interno_nueva_editar").val();
    var telefono = $("#txt_numinterno_editar").val();
    if(internoactual.length==0 || internonueva.length==0 || telefono.length==0){
        return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning");
    }

    $.ajax({
        "url":"../controlador/interno/controlador_interno_modificar.php",
         type:'POST',
         data:{
            id:id,
            internoac:internoactual,
            internonu:internonueva,
            telefono:telefono
         },
    }).done(function(resp){
        if(resp>0){
               if(resp==1){
                $("#modal_editar").modal('hide');
                listar_interno();
                Swal.fire("Mensaje de confirmacion", "Datos actualizados", "success");
                }else{
                Swal.fire("Mensaje de advertencia", "La interno ya existe en la base de datos", "warning");
                }
        }else{
           
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}