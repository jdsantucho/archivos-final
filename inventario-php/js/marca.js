var tablemarcas;
function listar_marca(){
        tablemarcas = $("#tabla_marca").DataTable({
        "ordering":true,
        "bLengthChange":false,
        "paging": true,
        "searching": { "regex": true },
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "pageLength": 10,
        "destroy":true,
        "async": false ,
        "processing": true,
        "ajax":{
            "url":"../controlador/marca/controlador_marca_listar.php",
            type:'POST'
        },
        "order":[[1, 'asc']],
        "columns":[
            {"defaultContent":""},
            {"data":"marca_nombre"},
            {"data":"marca_fregistro"},
            {"data":"marca_estatus",
            render: function (data, type, row ) {
                if(data=='ACTIVO'){
                    return "<span class='label label-success'>"+data+"</span>";                   
                }if(data=='INACTIVO'){
                    return "<span class='label label-danger'>"+data+"</span>";                   
                }if(data=='AGOTADO'){
                    return "<span class='label label-black' style='background:black'>"+data+"</span>";                   
                }
              }
            },  
            {"defaultContent":"<button style='font-size:13px;' type='button' class='editar btn btn-primary'><i class='fa fa-edit'></i></button>"}
        ],

        "language":idioma_espanol,
        "columnDefs": [
                {className: "dt-center", "targets": [0,1, 2, 3,4]
                }
            ],
        select: true
    });
    document.getElementById("tabla_marca_filter").style.display="none";
    $('input.global_filter').on('keyup click',function(){
        filterGlobal();
    });
    $('input.column_filter').on('keyup click',function(){
        filterColumn($(this).parents('tr').attr('data-column'));
    });

    tablemarcas.on('draw.dt', function(){
        var PageInfo = $('#tabla_marca').DataTable().page.info();
        tablemarcas.column(0, { page: 'current'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

}

function filterGlobal(){
    $('#tabla_marca').DataTable().search(
        $('#global_filter').val(),
    ).draw();
}

function Registrar_Marca(){
    var marca = $("#txt_marca").val();
    var estatus = $("#cbm_estatus").val();


    if(marca.length==0 || estatus.length== 0){
        return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning");
    }

    $.ajax({
        "url":"../controlador/marca/controlador_marca_registro.php",
         type:'POST',
         data:{
            marca:marca,
            estatus:estatus
         }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                listar_marca();
                LimpiarCampos();
                $("#modal_registro").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Marca registrada", "success");
                }else{
                LimpiarCampos();
                Swal.fire("Mensaje de advertencia", "La marca ya existe en la base de datos", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}

function LimpiarCampos(){
    $("#txt_marca").val("");
}

$('#tabla_marca').on('click','.editar',function(){
    var data = tablemarcas.row($(this).parents('tr')).data();//Detecta a que fila hago click y captura los datos en la variable data
    if(tablemarcas.row(this).child.isShown()){//Cuando esta en tamaño responsivo
        var data = tablemarcas.row(this).data();
    }
    $("#modal_editar").modal({backdrop:'static',keyboard:false});
    $("#modal_editar").modal('show');
    $("#id_marca").val(data.marca_id);
    $("#txt_marca_actual_editar").val(data.marca_nombre);
    $("#txt_marca_nueva_editar").val(data.marca_nombre);
    $("#cbm_estatus_editar").val(data.marca_estatus).trigger("change");
})

function Editar_Marca(){
    var id = $("#id_marca").val();
    var marcaactual = $("#txt_marca_actual_editar").val();
    var marcanueva = $("#txt_marca_nueva_editar").val();
    var estatus = $("#cbm_estatus_editar").val();


    if(marcaactual.length==0 || marcanueva.length==0 || estatus.length== 0){
        return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning");
    }

    $.ajax({
        "url":"../controlador/marca/controlador_marca_modificar.php",
         type:'POST',
         data:{
            id:id,
            marcaac:marcaactual,
            marcanu:marcanueva,
            estatus:estatus
         },
    }).done(function(resp){
        if(resp>0){
               if(resp==1){
                $("#modal_editar").modal('hide');
                listar_marca();
                Swal.fire("Mensaje de confirmacion", "Datos actualizados", "success");
                }else{
                Swal.fire("Mensaje de advertencia", "La marca ya existe en la base de datos", "warning");
                }
        }else{
           
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}