var tablepropietario;
function listar_propietario(){
        tablepropietario = $("#tabla_propietario").DataTable({
        "ordering":true,
        "bLengthChange":false,
        "paging": true,
        "searching": { "regex": true },
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "pageLength": 10,
        "destroy":true,
        "async": false ,
        "processing": true,
        "ajax":{
            "url":"../controlador/propietario/controlador_propietario_listar.php",
            type:'POST'
        },
        "order":[[1, 'asc']],
        "columns":[
            {"defaultContent":""},
            {"data":"propietario_nombre"},
            {"data":"propietario_fregistro"},
            {"data":"propietario_estatus",
            render: function (data, type, row ) {
                if(data=='ACTIVO'){
                    return "<span class='label label-success'>"+data+"</span>";                   
                }if(data=='INACTIVO'){
                    return "<span class='label label-danger'>"+data+"</span>";                   
                }if(data=='AGOTADO'){
                    return "<span class='label label-black' style='background:black'>"+data+"</span>";                   
                }
              }
            },  
            {"defaultContent":"<button style='font-size:13px;' type='button' class='editar btn btn-primary'><i class='fa fa-edit'></i></button>"}
        ],

        "language":idioma_espanol,
        "columnDefs": [
                {className: "dt-center", "targets": [0,1, 2, 3,4]
                }
            ],
        select: true
    });
    document.getElementById("tabla_propietario_filter").style.display="none";
    $('input.global_filter').on('keyup click',function(){
        filterGlobal();
    });
    $('input.column_filter').on('keyup click',function(){
        filterColumn($(this).parents('tr').attr('data-column'));
    });

    tablepropietario.on('draw.dt', function(){
        var PageInfo = $('#tabla_propietario').DataTable().page.info();
        tablepropietario.column(0, { page: 'current'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

}

function filterGlobal(){
    $('#tabla_propietario').DataTable().search(
        $('#global_filter').val(),
    ).draw();
}

function Registrar_Propietario(){
    var propietario = $("#txt_propietario").val();
    var estatus = $("#cbm_estatus").val();


    if(propietario.length==0 || estatus.length== 0){
        return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning");
    }

    $.ajax({
        "url":"../controlador/propietario/controlador_propietario_registro.php",
         type:'POST',
         data:{
            propietario:propietario,
            estatus:estatus
         }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                listar_propietario();
                LimpiarCampos();
                $("#modal_registro").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Propietario registrada", "success");
                }else{
                LimpiarCampos();
                Swal.fire("Mensaje de advertencia", "La propietario ya existe en la base de datos", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}

function LimpiarCampos(){
    $("#txt_propietario").val("");
}

$('#tabla_propietario').on('click','.editar',function(){
    var data = tablepropietario.row($(this).parents('tr')).data();//Detecta a que fila hago click y captura los datos en la variable data
    if(tablepropietario.row(this).child.isShown()){//Cuando esta en tamaño responsivo
        var data = tablepropietario.row(this).data();
    }
    $("#modal_editar").modal({backdrop:'static',keyboard:false});
    $("#modal_editar").modal('show');
    $("#id_propietario").val(data.propietario_id);
    $("#txt_propietario_actual_editar").val(data.propietario_nombre);
    $("#txt_propietario_nueva_editar").val(data.propietario_nombre);
    $("#cbm_estatus_editar").val(data.propietario_estatus).trigger("change");
})

function Editar_Propietario(){
    var id = $("#id_propietario").val();
    var propietarioactual = $("#txt_propietario_actual_editar").val();
    var propietarionueva = $("#txt_propietario_nueva_editar").val();
    var estatus = $("#cbm_estatus_editar").val();


    if(propietarioactual.length==0 || propietarionueva.length==0 || estatus.length== 0){
        return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning");
    }

    $.ajax({
        "url":"../controlador/propietario/controlador_propietario_modificar.php",
         type:'POST',
         data:{
            id:id,
            propietarioac:propietarioactual,
            propietarionu:propietarionueva,
            estatus:estatus
         },
    }).done(function(resp){
        if(resp>0){
               if(resp==1){
                $("#modal_editar").modal('hide');
                listar_propietario();
                Swal.fire("Mensaje de confirmacion", "Datos actualizados", "success");
                }else{
                Swal.fire("Mensaje de advertencia", "La propietario ya existe en la base de datos", "warning");
                }
        }else{
           
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}