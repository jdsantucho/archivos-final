### Final - Actualización tecnológica 

# Dockerfile - Kubernetes

![](https://static.wixstatic.com/media/96edd2_c80f2a087a3144fdbe26718a3121a4d6~mv2.png)

### Indice
1.  Contenido
2.  APP Inventario
3. Dockerfile
4. Subir imagen docker a DOCKERHUB
5. Kubernetes
6. Ingress



## Contenido 

- [x] inventario-php
- [ ] entrypoint.sh
- [ ] Dockerfile
- [ ] default.conf
- [x] kubernetes
    - [ ] php-dp.yaml
    - [ ] pv.yaml
    - [x] ingress
        - [ ] nginx-controller.yaml
        - [ ] nginx-controller-svc.yaml
        - [ ]  ingress-rules.yaml
        - [ ]  ingress-example.yaml

### APP Inventario


![](https://i.ibb.co/617QNYK/Deploy.png)

usuario:admin
contraseña: 1234

## Dockerfile

    Se creará una imagen docker con el trabajo del TP de la APP DINAMICA
	https://gitlab.com/jdsantucho/archivos-final

    Link de la imagen publicada
	https://hub.docker.com/r/jdsantucho/final

###### Para descargar el repositorio de GITLAB

1-	Crear carpeta de trabajo
```
$ vm@cloud-01:~$ mkdir inventario
```


2-	Moverse a la carpeta creada
```
vm@cloud-01:~$ cd inventario
```

3-	Para descargar el contenido del repositorio
```
vm@cloud-01:~/inventario$ git clone https://gitlab.com/jdsantucho/archivos-final.git
```
4-	Una vez completada la descarga, nos dirigimos a la carpeta creada
```
vm@cloud-01:~/inventario$ cd archivos-final/
```
5-	Dentro de esta carpeta tenemos los archivos necesarios para crear la imagen de docker, esto se puede ver con el comando ls

```
vm@cloud-01:~/inventario/archivos-final$ ls
default.conf  Dockerfile  entrypoint.sh  inventario-php
```

Comandos para crear la imagen de docker
Ejemplo:
```
vm@cloud-01:~/inventario/archivos-final$  docker build -t inventario .
```
Donde inventario es el nombre que le pondremos a la imagen, y el signo . (punto) hace referencia a la ruta donde nos encontramos, en este caso, se buscará el archivo Dockerfile en la ruta : /inventario/archivos-final

Al terminar el proceso, podemos listar las imágenes docker con el comando 
```
vm@cloud-01:~/inventario/archivos-final$  docker images
```
|  REPOSITORY | TAG  | IMAGE ID  | CREATED   | SIZE   |
| :------------: | :------------: | :------------: | :------------: | :------------: |
| inventario  |  latest | a5e525261f33  | About a minute ago   | 957MB  |

###### Para subir imagen a DOCKERHUB

Se debe crear una cuenta para poder subir nuestra imagen 
Link: https://hub.docker.com/

Una vez registrados, vamos a CREATE REPOSITORY


 ![](https://i.ibb.co/6DTB6Y5/crearrepositorio.png)


En esta sección registramos nombre de imagen, alguna descripción y la visibilidad. Para finalizar CREATE


 ![](https://i.ibb.co/55J43Gb/formcrear.png)


 
Confirmada la operación, vemos que el comando para subir la imagen es jdsantucho/final es decir NOMBREDEUSUARIO/NOMBREDEIMAGEN


![](https://i.ibb.co/v4vz9sb/formok.png)


Volviendo a la terminal, debemos loguearnos en nuestro host de docker
```
vm@cloud-01:~/inventario/archivos-final$ docker login -u NOMBREDEUSUARIO
```
Ejemplo:
```
vm@cloud-01:~/inventario/archivos-final$ docker login -u jdsantucho
```
```
Password:
```

Login Succeeded
 

Ejemplo del comando para subir imagen docker 
```
vm@cloud-01:~/inventario/archivos-final$ docker push jdsantucho/final
```
Es importante saber que el nombre de la imagen a subir debe crearse con usuario/nombre imagen 

Ejemplo:
```
docker build -t inventario .
```
```
docker build -t jdsantucho/final .
```



## Kubernetes

Archivos de deploy 
https://gitlab.com/jdsantucho/archivos-final/-/tree/master/kubernetes
Para el deploy de esta APP, crearemos un volumen persistente, cuando un POD se muere, también lo hace la información dentro del mismo. Con un volumen persistente logramos guardar esa información o los cambios efectuados desde el nacimiento del POD.

Archivo pv.yaml

```
apiVersion: v1
kind: PersistentVolume  # Tipo 
metadata:
  name: mysql-pv-volume # Nombre del PV
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 20Gi       # Espacio a reservar
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/inventario" # Ruta de donde se almacenan los datos del PV
---
apiVersion: v1
kind: PersistentVolumeClaim #Reclamo del PV
metadata:
  name: mysql-pv-claim #Nombre del reclamo 
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce # Comportamiento 
  resources:
    requests:
      storage: 1Gi # Espacio a reclamar
```



```
root@docker:~/1/archivos-final/kubernetes# kubectl apply -f pv.yaml
```

*Mensaje de confirmación:
persistentvolume/mysql-pv-volume created
persistentvolumeclaim/mysql-pv-claim created*

Crear carpeta para el PV
```
root@docker:~/1/archivos-final/kubernetes# mkdir -p /mnt/inventario
```
Archivo php-deploy.yaml

```
apiVersion: v1
kind: Service
metadata:
  name: inventario
spec:
  selector:
    app: inventario
    tier: frontend
  ports:
  - nodePort: 31600 # Puerto para accede al servicio
    port: 80
    protocol: TCP
    targetPort: 80
  type: NodePort
---
apiVersion: apps/v1
kind: Deployment #Tipo
metadata:
  name: inventario #Nombre del pod
  labels:
    app: inventario
    tier: frontend
spec:
  selector:
    matchLabels:
      app: inventario
      tier: frontend
  replicas: 1
  template:
    metadata:
      labels:
        app: inventario
        tier: frontend
    spec:
      containers:
      - name: inventario
        image: jdsantucho/final #Imagen casera creada
        env: #Variables de entorno para la imagen
        - name: MYSQL_USER_PASSWORD 
          value: password
        - name: MYSQL_DB_NAME
          value: inventario
        - name: MYSQL_USER
          value: bduser
        volumeMounts:
        - name: mysql-persistent-storage
          mountPath: /var/www/html # Ruta de los datos a persistir
      volumes:
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: mysql-pv-claim # Nombre identificatorio del reclamo de PV
```
```
root@docker:~/1/archivos-final/kubernetes# kubectl apply -f php-dp.yaml
```

*Mensaje de confirmación:
service/inventario created
deployment.apps/inventario created*

Comando para ver el estado de los pods
```
root@docker:~# kubectl get pods
```

|  NAME | READY  |  STATUS  | RESTARTS  | AGE   |
| :------------: | :------------: | :------------: | :------------: | :------------: |
| inventario-85894bb8d5-hwccr   | 0/1  | ContainerCreating  | 0   | 43m  |

Cuando el POD se encuentra listo, se ve en estado RUNNING
```
root@docker:~# kubectl get pods
```
|  NAME | READY  |  STATUS  | RESTARTS  | AGE   |
| :------------: | :------------: | :------------: | :------------: | :------------: |
| inventario-85894bb8d5-hwccr   | 1/1  | Running  | 0   | 5m  |


Comando para ver los servicios 
```
root@docker:~# kubectl get svc
```
| NAME  | TYPE  | CLUSTER-IP  | EXTERNAL-IP  |PORT(S)   |AGE   |
| :------------: | :------------: | :------------: | :------------: | :------------: | :------------: |
| kubernetes  | ClusterIP  | 10.152.183.1  | <none>  | 443/TCP  |  4d6h  |
| inventario  | NodePort  | 10.152.183.228  | <none>  | 80:31600/TCP  | 19s  |



Para acceder al deploy:
http://192.168.0.100:31600

Donde 192.168.0.100 es la IP del nodo

http://host:31600


## Algunos comandos para kubernetes

Comando para ver los volúmenes persistentes
```
root@docker:~# kubectl get pv
```
Comando para ver los volúmenes persistentes claim
```
root@docker:~# kubectl get pvc
```

### Ingress

ir a la carpeta archivos-final/kubernetes/ingress

```
cd archivos-final/kubernetes/ingress
```

Crear Ingress controller

Recordemos que es el componente encargado de configurar el enrutamiento para redirigir el tráfico
```
kubectl apply –f nginx-controller.yaml
```
Para ver el pod de nginx
```
kubectl get pods –n ingress-nginx
```
Este pod es el encargado de aplicar los permisos, de modificar la configuración de nginx, y de aplicar rutas dinámicas.

Pero hasta ahora no hay ningún servicio que expuesto, para eso, debemos crear un servicio de ingress controller de tipo NODEPORT
```
kubectl apply –f nginx-controller-svc.yaml
```
```
kubectl get svc –n ingress-nginx
```
Aca chequeamos el puerto creado para acceder al cluster, por ejemplo en mi caso de mi LOCAL-IP:PUERTO
192.168.0.100:30783

Para entornos de testing/desarrollo, modificar archivo hosts, agregar ruta
```
*192.168.0.100  app1.mydomain.com*
 ```

Ahora vamos a crear las reglar para dirigir el trafico
```
kubectl apply –f ingress-rules.yaml
```
Este archivo redirigirá la ruta app1.mydomain.com hacia el servicio de la web inventario

![](https://i.ibb.co/tCKynwT/hosts.png)

usuario:admin
contraseña: 1234
